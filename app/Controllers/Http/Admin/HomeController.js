'use strict'
const User = use('App/Models/User')

class HomeController {
    /**
     * Show Dashboard Home
     * 
     * @param {view} param
     * 
     * @returns view
     */
    async index({ view }) {
        const data = [];

        data['title'] = 'Dashboard'
        data['userCount'] = await User.getCount()

        return view.render('home', { data })
    }
}

module.exports = HomeController
