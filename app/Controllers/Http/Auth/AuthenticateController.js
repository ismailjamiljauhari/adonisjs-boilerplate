'use strict'

class AuthenticateController {

    /**
     * Show Login Form
     * 
     * @param {view} param
     * 
     * @returns view
     */
    index({ view }) {
        const data = [];
        
        data['title'] = 'Login';

        return view.render('auth.login', { data })
    }

    /**
     * 
     * @param {auth, request, response} 
     * @returns response
     */
    async login({ auth, request, response, session }) {
        const { email, password } = request.only(['email', 'password'])
        
        await auth.logout()
        await auth.attempt(email, password)
        session.put('status', 'Anda berhasil login')

        return response.route('home')
    }
    
    /**
     * for Logut 
     * 
     * @return response
     */
    async logout({auth, response}) {

        try {
            await auth.check()
            await auth.logout()

            return response.route('login')
        } catch (error) {
            return response.route('login')
        }
    }   
    
}

module.exports = AuthenticateController
