const { hooks } = require('@adonisjs/ignitor')


hooks.after.providersBooted(() => {
    const View = use('View')
    View.global('currentTime', function (param = null ) {
        var timestamp = require('time-stamp');
        
        return param ? timestamp(param) : timestamp();
        return new Date().getTime()
    })

    View.global('config', function (key) {
        const Config = use('Config')
        const response = Config.get(key)
        
        return response
    })
})