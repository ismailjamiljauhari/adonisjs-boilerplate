'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', async ({ response }) => {
    response.redirect('/login')
})

Route.get('login', 'Auth/AuthenticateController.index').as('login.index')
Route.post('login', 'Auth/AuthenticateController.login').as('login.post')
Route.get('logout', 'Auth/AuthenticateController.logout').as('logout').middleware('auth')

//Admin Route
Route.group(() => {
    Route.get('home', 'Admin/HomeController.index').as('home')

    Route.resource('users', 'Admin/UserController')
    Route.get('/users/data/json', 'Admin/UserController.getData').as('users.data');
}).prefix('admin').middleware(['auth'])
